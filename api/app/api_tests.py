import unittest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from app.routers import states, supportgroups, professionals


class TestAPIEndpoints(unittest.TestCase):
    def setUp(self):
        # Setup a test client for the FastAPI application
        
        app = FastAPI()
        
        app.include_router(states.router)
        app.include_router(professionals.router)
        app.include_router(supportgroups.router)
        
        self.client = TestClient(app)


    # Existing professionals tests
    async def test_get_all_professionals(self):
        with self.client:
            response = self.client.get("/professionals/")
            self.assertEqual(response.status_code, 200)

    async def test_get_professionals_metadata(self):
        with self.client:
            response = self.client.get("/professionals/metadata")
            self.assertEqual(response.status_code, 200)

    async def test_get_professional_by_id(self):
        with self.client:
            valid_uuid = 'acb3007b-2fc7-49b2-a834-ad73747ccff1'
            response = self.client.get(f"/professionals/{valid_uuid}")
            self.assertIn(response.status_code, (200, 404))

    async def test_get_professionals_by_state(self):
        with self.client:
            response = self.client.get("/professionals/state/Texas")
            self.assertEqual(response.status_code, 200)

    async def test_get_all_supportgroups(self):
        with self.client:
            response = self.client.get("/supportgroups/")
            self.assertEqual(response.status_code, 200)
            
    async def test_get_supportgroups_metadata(self):
        with self.client:
            response = self.client.get("/supportgroups/metadata")
            self.assertEqual(response.status_code, 200)

    async def test_get_supportgroup_by_id(self):
        with self.client:
            valid_uuid = '593e4fd7-a653-43e8-9790-796efd5b663d'  
            response = self.client.get(f"/supportgroups/{valid_uuid}")
            self.assertIn(response.status_code, (200, 404))

    async def test_get_supportgroups_by_state(self):
        with self.client:
            response = self.client.get("/supportgroups/state/Texas")
            self.assertEqual(response.status_code, 200)

    async def test_get_all_states(self):
        with self.client:
            response = self.client.get("/states/?limit=10&page=1")
            self.assertEqual(response.status_code, 200)

    async def test_get_states_metadata(self):
        with self.client:
            response = self.client.get("/states/metadata")
            self.assertEqual(response.status_code, 200)

    async def test_get_state_by_name(self):
        with self.client:
            state_name = 'Texas'  
            response = self.client.get(f"/states/{state_name}")
            self.assertIn(response.status_code, (200, 404))

if __name__ == '__main__':
    unittest.main()
