from uuid import UUID
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import APIRouter, HTTPException, Depends
from app.dependencies import get_db, get_params

router = APIRouter(
    prefix="/supportgroups",
    tags=["supportgroups"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def getAll(
    query: dict[str, int] = Depends(get_params), session: AsyncSession = Depends(get_db)
):
    res = await session.execute(
        text(
            "SELECT * from groups ORDER BY name LIMIT :limit OFFSET :offset"
        ).bindparams(limit=query["limit"], offset=query["offset"])
    )
    return [x._asdict() for x in res.all()]


@router.get("/metadata")
async def metadata(
    query: dict[str, int] = Depends(get_params), session: AsyncSession = Depends(get_db)
):
    res = await session.execute(text("SELECT COUNT(*) from groups"))
    return {"count": res.scalar(), "limit": query["limit"]}


@router.get("/{ID}")
async def get(ID: UUID, session: AsyncSession = Depends(get_db)):
    data = (
        await session.execute(
            text("SELECT * from groups WHERE id=:id ORDER BY name").bindparams(id=ID)
        )
    ).first()

    if not data:
        raise HTTPException(status_code=404, detail="Not found :(")

    return data._asdict()


@router.get("/state/{name}")
async def getByState(name: str, session: AsyncSession = Depends(get_db)):
    res = await session.execute(
        text("SELECT * from groups WHERE state=:name ORDER BY name").bindparams(
            name=name
        )
    )

    data = [x._asdict() for x in res.all()]

    if not data:
        raise HTTPException(status_code=404, detail="Not found :(")

    return data
